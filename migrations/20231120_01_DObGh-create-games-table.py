"""
create games table
"""

from yoyo import step

__depends__ = {}

steps = [
    step(
        """
            create table if not exists games(
                id serial primary key,
                title text not null,
                release int not null,
        
                check ( length(title) > 0 ),
                check ( release > 1957 ),
                check ( release < 3000 )
            )
        """,
        """
            drop table if exists games; 
        """,
    )
]
