# CODE EXAMPLE

Template of one of the microservices I developed
## Stack

- python:3.9, FastAPI, asyncio, pydantic
- ООП, patterns
- PostgreSQL, aiopg
- Redis
- bash, yoyo migrations, poetry
- docker, docker-compose

# How to run the project

- [ ]  Clone the repository:
```bash
git clone git@gitlab.com:Glebserg/code_example.git
```
- [ ] Copy* `.env.example` to `.env`
```bash
cp .env.example .env
```
> (*) - if on Windows, then
```bash
copy .env.example .env
```
- [ ] Edit the file if you needed (e.g., choose ports)
- [ ] Launch command
```bash
docker compose -f docker-compose_dev.yaml up --build -d
```
After the launch, visit [127.0.0.1:5000/docs](http://127.0.0.1:5000/docs) `(localhost:${API__PORT}/docs)` to view the Swagger schema.

## Summary
Four containers will be launched: api, postgres, redis, migration in one Docker network and will communicate using container names.
A record will be added to the database, and you can interact with the API.