"""This package contains all the handlers for the application.

In this package, you can store handlers that help you inherit and extend
native python logic.
"""

from .recursive_attr import *
