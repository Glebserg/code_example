from pydantic.fields import Field
from pydantic.types import PositiveInt

from app.pkg.models.base import BaseModel

__all__ = [
    "Game",
    "GameFields",
    "CreateGameCommand",
    "ReadGameByIdQuery",
    "ReadGameByGameTitleQuery",
    "UpdateGameCommand",
    "DeleteGameCommand",
]


class GameFields:
    id = Field(description="Game id.", example=42)
    title = Field(description="Game title", example="Super Game")
    release = Field(description="Release date", example=2023)


class BaseGame(BaseModel):
    """Base model for game."""


class Game(BaseGame):
    id: PositiveInt = GameFields.id
    title: str = GameFields.title
    release: PositiveInt = GameFields.release


# Commands.
class CreateGameCommand(BaseGame):
    title: str = GameFields.title
    release: PositiveInt = GameFields.release


class UpdateGameCommand(BaseGame):
    id: PositiveInt = GameFields.id
    title: str = GameFields.title
    release: PositiveInt = GameFields.release


class DeleteGameCommand(BaseGame):
    id: PositiveInt = GameFields.id


# Query
class ReadGameByGameTitleQuery(BaseGame):
    title: str = GameFields.title


class ReadGameByIdQuery(BaseGame):
    id: PositiveInt = GameFields.id
