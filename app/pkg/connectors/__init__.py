"""All connectors in declarative container."""

from dependency_injector import containers, providers

from .postgresql import PostgresSQL
from .resourсes import BaseAsyncResource

__all__ = ["Connectors", "PostgresSQL"]


class Connectors(containers.DeclarativeContainer):
    """Declarative container with connectors."""

    postgresql: PostgresSQL = providers.Container(PostgresSQL)
