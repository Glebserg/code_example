from typing import List

from dependency_injector.wiring import Provide, inject
from fastapi import APIRouter, Depends, status

from app.internal.services import Services
from app.internal.services.game import GameService
from app.pkg import models

router = APIRouter(prefix="/game", tags=["Game"])


@router.post(
    "/",
    response_model=models.Game,
    status_code=status.HTTP_201_CREATED,
    description="Create game",
)
@inject
async def create_game(
    cmd: models.CreateGameCommand,
    game_service: GameService = Depends(Provide[Services.game_service]),
):
    return await game_service.create_game(cmd=cmd)


@router.get(
    "/",
    response_model=List[models.Game],
    status_code=status.HTTP_200_OK,
    description="Get all games",
)
@inject
async def read_all_games(
    game_service: GameService = Depends(Provide[Services.game_service]),
):
    return await game_service.read_all_games()


@router.get(
    "/{game_id:int}",
    response_model=models.Game,
    status_code=status.HTTP_200_OK,
    description="Read specific game",
)
@inject
async def read_game(
    game_id: int,
    game_service: GameService = Depends(Provide[Services.game_service]),
):
    return await game_service.read_specific_game_by_id(
        query=models.ReadGameByIdQuery(id=game_id),
    )


@router.delete(
    "/{game_id:int}",
    response_model=models.Game,
    status_code=status.HTTP_200_OK,
    description="Delete specific game",
)
@inject
async def delete_game(
    game_id: int,
    game_service: GameService = Depends(Provide[Services.game_service]),
):
    return await game_service.delete_specific_game(
        cmd=models.DeleteGameCommand(id=game_id),
    )
