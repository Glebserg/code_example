"""Global point for collected routers."""

from app.internal.pkg.models import Routes
from app.internal.routes import game

__all__ = ["__routes__"]


__routes__ = Routes(routers=(game.router,))
"""Global point for collected routers.

This snippet from app/internal/pkg/models/routes.py:

Examples:
    When you using routers with `FastAPI`::
        >>> from fastapi import FastAPI
        >>> app = FastAPI()
        >>> __routes__.register_routes(app=app)
"""
