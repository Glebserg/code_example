from dependency_injector import containers, providers

from app.internal.repository import Repositories, postgresql
from app.internal.services import game
from app.internal.services.game import GameService


class Services(containers.DeclarativeContainer):
    """Containers with services."""

    repositories: postgresql.Repositories = providers.Container(
        Repositories.postgres,
    )  # type: ignore

    game_service = providers.Factory(GameService, repositories.game_repository)
