"""Game service."""
from typing import List

from app.internal.repository.postgresql import GameRepository
from app.pkg import models

__all__ = ["GameService"]


# TODO: add exceptions
class GameService:
    repository: GameRepository

    def __init__(self, game_repository: GameRepository):
        self.repository = game_repository

    async def create_game(self, cmd: models.CreateGameCommand) -> models.Game:
        """Function for create game. Game password will be encrypted.

        Args:
            cmd: `CreateGameCommand`.

        Raises:
            GameAlreadyExist: when game title of game already taken in repository.

        Returns: `Game` model.
        """

        return await self.repository.create(cmd=cmd)

    async def read_all_games(self) -> List[models.Game]:
        """Read all games from repository.

        Returns:
            List of `Game` models.
        """

        return await self.repository.read_all()

    async def read_specific_game_by_game_title(
        self,
        query: models.ReadGameByGameTitleQuery,
    ) -> models.Game:
        """Read specific game from repository by game title.

        Args:
            query: `ReadGameByGameNameQuery`.

        Returns: `Game` model.
        """

        return await self.repository.read_by_game_title(query=query)

    async def read_specific_game_by_id(
        self,
        query: models.ReadGameByIdQuery,
    ) -> models.Game:
        """Read specific game from repository by game id.

        Args:
            query: `ReadGameByIdQuery`.

        Returns: `Game` model.
        """

        return await self.repository.read(query=query)

    async def delete_specific_game(self, cmd: models.DeleteGameCommand) -> models.Game:
        """Delete specific game by game id.

        Args:
             cmd: `DeleteGameCommand`.

        Returns: `Game` model.
        """

        return await self.repository.delete(cmd=cmd)
