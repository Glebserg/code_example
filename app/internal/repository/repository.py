from abc import ABC
from typing import List, TypeVar

from app.pkg.models.base import Model

__all__ = ["Repository", "BaseRepository"]


BaseRepository = TypeVar("BaseRepository", bound="Repository")


class Repository(ABC):
    """Base repository interface.

    All repositories must implement this interface.

    Examples:
        >>> from app.pkg.models.game import Game
        >>> from app.pkg.models.game import (
        ...     CreateGameCommand,
        ...     UpdateGameCommand,
        ...     DeleteGameCommand,
        ...     ReadGameByIdQuery,
        ... )
        >>> class GameRepository(Repository):
        ...     async def create(self, cmd: CreateGameCommand) -> Game:
        ...         ...
        ...
        ...     async def read(self, query: ReadGameByIdQuery) -> Game:
        ...         ...
        ...
        ...     async def read_all(self) -> List[Game]:
        ...         ...
        ...
        ...     async def update(self, cmd: UpdateGameCommand) -> Game:
        ...         ...
        ...
        ...     async def delete(self, cmd: DeleteGameCommand) -> Game:
        ...         ...

    Notes:
        All methods must be asynchronous.

    Warnings:
        1. You must use ``query`` for search model in database and ``cmd`` for create,
            update and delete model in database.
        2. ``query`` and ``cmd`` must be inherited from ``Model`` returning type.
        3. Delete method must return **MARKED** row for delete.  It is necessary for
            correct work of the repository layer. Repository cant delete row from
            database. It can only mark row as deleted.
        4. All methods must return model contains all fields.
    """

    async def create(self, cmd: Model) -> Model:
        """Create model.

        Args:
            cmd (Model): Specific command for create model. Must be inherited from
                ``Model``.

        Returns:
            Type of the parent model.
        """
        raise NotImplementedError

    async def read(self, query: Model) -> Model:
        """Read model.

        Args:
            query (Model): Specific query for read model. Must be inherited from
                ``Model``.

        Returns:
            Type of the parent model.
        """

        raise NotImplementedError

    async def read_all(self) -> List[Model]:
        """Read all rows."""

        raise NotImplementedError

    async def update(self, cmd: Model) -> Model:
        """Update model.

        Notes: In this method cmd must contain id of the model for update and ALL
        fields for update.

        Returns:
            Type of the parent model.
        """

        raise NotImplementedError

    async def delete(self, cmd: Model) -> Model:
        """Delete model.

        Notes: In this method you should mark row as deleted. You must not delete row
            from database.

        Returns:
            Type of the parent model.
        """

        raise NotImplementedError
