"""Module ``Repository`` must contain **CRUD** methods.

Note:
    The repository must contain a minimum set of instructions for interacting with the
    target database.

Examples:
    Let's say we have a User repository. For its correct operation, 5 implemented
    methods are sufficient.::

        >>> from app.internal.repository.repository import Repository
        >>> from typing import List
        >>> from app.pkg.models.game import (
        ...     CreateGameCommand,
        ...     DeleteGameCommand,
        ...     UpdateGameCommand,
        ...     ReadGameByGameTitleQuery,
        ...     Game,
        ... )
        ...
        >>> class UserRepository(Repository):
        ...     async def create(self, cmd: CreateGameCommand) -> Game:
        ...         ...
        ...
        ...     async def read(self, query: ReadGameByGameTitleQuery) -> Game:
        ...         ...
        ...
        ...     async def read_all(self) -> List[Game]:
        ...         ...
        ...
        ...     async def update(self, cmd: UpdateGameCommand) -> Game:
        ...         ...
        ...
        ...     async def delete(self, cmd: DeleteGameCommand) -> Game:
        ...         ...
"""

from dependency_injector import containers, providers

from . import postgresql
from .repository import BaseRepository

__all__ = ["Repositories"]


class Repositories(containers.DeclarativeContainer):
    postgres = providers.Container(postgresql.Repositories)
