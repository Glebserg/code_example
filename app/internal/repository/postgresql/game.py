from typing import List

from app.internal.repository.postgresql.connection import get_connection
from app.internal.repository.postgresql.handlers.collect_response import (
    collect_response,
)
from app.internal.repository.repository import Repository
from app.pkg import models

__all__ = ["GameRepository"]


class GameRepository(Repository):
    @collect_response
    async def create(self, cmd: models.CreateGameCommand) -> models.Game:
        q = """
            insert into games(
                title, release
            ) values (
                %(title)s,
                %(release)s
            )
            returning id, title, release;
        """
        async with get_connection() as cur:
            await cur.execute(q,  cmd.to_dict())
            return await cur.fetchone()

    @collect_response
    async def read(self, query: models.ReadGameByIdQuery) -> models.Game:
        q = """
            select
                id,
                title,
                release
            from games
            where games.id = %(id)s;
        """
        async with get_connection() as cur:
            await cur.execute(q, query.to_dict())
            return await cur.fetchone()

    @collect_response
    async def read_by_game_title(
        self,
        query: models.ReadGameByGameTitleQuery,
    ) -> models.Game:
        q = """
            select
                id, title, release
            from games
            where title = %(title)s
        """
        async with get_connection() as cur:
            await cur.execute(q, query.to_dict())
            return await cur.fetchone()

    @collect_response
    async def read_all(self) -> List[models.Game]:
        q = """
            select
                id,
                title,
                release
            from games
        """
        async with get_connection() as cur:
            await cur.execute(q)
            return await cur.fetchall()

    @collect_response
    async def update(self, cmd: models.UpdateGameCommand) -> models.Game:
        q = """
            update games
                set
                    title = %(title)s,
                    release = %(release)s,
                where id = %(id)s
            returning
                id, title, release;
        """
        async with get_connection() as cur:
            await cur.execute(q, cmd.to_dict())
            return await cur.fetchone()

    @collect_response
    async def delete(self, cmd: models.DeleteGameCommand) -> models.Game:
        q = """
            delete from games where id = %(id)s
            returning id, title, release;
        """
        async with get_connection() as cur:
            await cur.execute(q, cmd.to_dict())
            return await cur.fetchone()
