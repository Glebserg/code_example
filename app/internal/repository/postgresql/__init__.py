from dependency_injector import containers, providers

from .game import GameRepository


class Repositories(containers.DeclarativeContainer):
    game_repository = providers.Factory(GameRepository)
